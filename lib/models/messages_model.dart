import 'package:flutter/cupertino.dart';
import 'package:messaging/models/conversation_model.dart';

class MessagesModel {

  final String avatar;
  final String content;
  final String username;
  final String conversationId;
  final String id;

  MessagesModel({required this.id, required this.avatar, required this.content, required this.username, required this.conversationId});

  factory MessagesModel.fromJson(Map json){
    return MessagesModel(id: json['_id'], avatar: json['avatar'], content: json['content'], username: json['username'], conversationId: json['conversationId']);
  }

}

class MessagesProvider extends ChangeNotifier{

  late List<MessagesModel> messages = [];
  late List<ConversationModel> currentConversation = [];

  setMessages(List<MessagesModel> messageList){
    this.messages = messageList;
    notifyListeners();
  }

  setConversation(List<ConversationModel> conversation){
    this.currentConversation = conversation;
    notifyListeners();
  }

}