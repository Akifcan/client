import 'package:flutter/cupertino.dart';

class UserModel extends ChangeNotifier{
  late final String username;
  late final String email;
  late  String avatar;
  late final String id;

  setUser(Map? user){
    this.username = user?['username'];
    this.email = user?['email'];
    this.avatar = user?['avatar'];
    this.id = user?['_id'];
  }

  changeAvatar(String avatar){
    this.avatar = 'avatars/$avatar.png';
    notifyListeners();
  }

}