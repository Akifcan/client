class ConversationModel{
  final String content;
  final bool isMe;

  ConversationModel({required this.content, required this.isMe});

}