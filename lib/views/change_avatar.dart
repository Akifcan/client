import 'dart:math';

import 'package:flutter/material.dart';
import 'package:messaging/constants/constants.dart';
import 'package:messaging/views/confirm_avatar.dart';

class ChangeAvatar extends StatelessWidget {
  const ChangeAvatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final int avatarCount = Random().nextInt(44) + 10;
    print(avatarCount);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Choose new avatar')
      ),
      body: GridView(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, mainAxisSpacing: 5, crossAxisSpacing: 5),
        children: [
          for(int i = avatarCount-10; i<=avatarCount; i++)
            GestureDetector(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => ConfirmAvatar(imagePath: '$i'))),
              child: Image.network(avatarPath('avatars/$i.png')))
        ],
      ),
    );
  }
}