import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:messaging/constants/constants.dart';
import 'package:messaging/globals.dart';
import 'package:messaging/models/messages_model.dart';
import 'package:messaging/models/user_model.dart';
import 'package:messaging/services/message_service.dart';
import 'package:messaging/theme.dart';
import 'package:messaging/views/settings.dart';
import 'package:messaging/widgets/app_header.dart';
import 'package:messaging/widgets/app_last_messages.dart';
import 'package:provider/provider.dart';
import 'package:connectivity_plus/connectivity_plus.dart';



class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  bool connectionInterupt = false;

  @override
  void initState() {
    super.initState();
    getMessages();
     Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
       if(result == ConnectivityResult.none){
         snackbarKey.currentState?.showSnackBar(SnackBar(content: Text("You don't have any internet connection"),  duration: Duration(seconds: 5))); 
        connectionInterupt = true;
       }else{
         if(connectionInterupt){
           snackbarKey.currentState?.showSnackBar(SnackBar(content: Text("Connected"),  duration: Duration(seconds: 10))); 
         }
       }
     });
  }

  getMessages() async {
    context.read<MessagesProvider>().setMessages(await MessageService.instance.listMessages());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        leading: Container(
          margin: EdgeInsets.symmetric(horizontal: 5),
          child: Consumer<UserModel>(
            builder: (_, model, __){
              return  CircleAvatar(
                  backgroundImage: NetworkImage(avatarPath(model.avatar)),
              );
            },
          )
        ),
        title: Text(context.read<UserModel>().username),
        actions: [
          IconButton(
            tooltip: 'Settings',
            onPressed: (){
              Navigator.of(context).push(MaterialPageRoute(builder: (_) => Settings()));
            }, icon: Icon(Icons.settings))
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [
            AppHeader(),
            Expanded(
              flex: 3,
              child: Container(
                padding: EdgeInsets.all(appPadding),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(appBorderRadius),
                    topRight: Radius.circular(appBorderRadius)
                  ),
                  color: Colors.white,
                ),
                child: Consumer<MessagesProvider>(
                  builder: (_, model, __){
                    return ListView.builder(
                      itemCount: model.messages.length,
                      itemBuilder: (_, index) => AppLastMessages(message: model.messages[index]),
                    );
                  },
                )
              ),
            ),
          ],
        ),
      ),
    );
  }
}