import 'package:flutter/material.dart';
import 'package:messaging/models/messages_model.dart';
import 'package:messaging/models/user_model.dart';
import 'package:messaging/services/message_service.dart';
import 'package:messaging/widgets/app_message_bubble.dart';
import 'package:provider/provider.dart';

class Messages extends StatefulWidget {
  final MessagesModel message;
  Messages({Key? key, required this.message}) : super(key: key);

  @override
  _MessagesState createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {

  TextEditingController message = TextEditingController();

  void _sendMessage() async {
    if(message.text.isNotEmpty){
      await MessageService.instance.sendMessage(widget.message.id, message.text);
      context.read<MessagesProvider>().setConversation(await MessageService.instance.listConversation(widget.message.conversationId, context.read<UserModel>().id, useDb: true));
      context.read<MessagesProvider>().setMessages(await MessageService.instance.listMessages(useDb: true));
      message.text = '';
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      context.read<MessagesProvider>().setConversation(await MessageService.instance.listConversation(widget.message.conversationId, context.read<UserModel>().id));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.message.username),
      ),
      body: Container(
        margin: const EdgeInsets.all(5),
        child: Consumer<MessagesProvider>(
          builder: (_, model, __){
            return ListView.builder(
              itemCount: model.currentConversation.length,
              itemBuilder: (_, index) {
                print(model.currentConversation[index].isMe);
                return AppMessageBubble(
                  alignment: model.currentConversation[index].isMe ? Alignment.centerRight : Alignment.centerLeft, isMe: model.currentConversation[index].isMe ? true : false, content: model.currentConversation[index].content);
              },
            );
          },
        )
      ),
      bottomNavigationBar: Container(
        margin: const EdgeInsets.all(10),
        child: SizedBox(
          height: 40,
          width: 40,
          child: Row(
            children: [
              Expanded(
                child: TextField(
                  controller: message,
                  decoration: InputDecoration(
                    hintText: 'Write here'
                  ),
                ),
              ),
              IconButton(
                tooltip: 'Send Message',
                color: Theme.of(context).primaryColor,
                onPressed: _sendMessage,
                icon: Icon(Icons.send),
              )
            ],
          )
        ),
      ),
    );
  }
}