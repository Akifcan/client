import 'package:flutter/material.dart';
import 'package:messaging/constants/constants.dart';
import 'package:messaging/helpers/helpers.dart';
import 'package:messaging/services/auth_service.dart';
import 'package:messaging/views/login.dart';
import 'package:messaging/views/register.dart';
import 'package:messaging/widgets/app_button.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Landing extends StatefulWidget {
  const Landing({Key? key}) : super(key: key);

  @override
  _LandingState createState() => _LandingState();
}

class _LandingState extends State<Landing> {
  void toLogin(BuildContext context){
    Navigator.of(context).push(newPage(Login(), -1.0));
  }

  void toRegister(BuildContext context){
    Navigator.of(context).push(newPage(Register(), 1.0));
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) { 
      AuthService.instance.autoLogin(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text('Chat', style: Theme.of(context).textTheme.headline1!.copyWith(
              color: Colors.white
            )),
            Text('With Your Friends', style: Theme.of(context).textTheme.headline5!.copyWith(color: Colors.white)),
            SizedBox(height: 50),
            AppButton(
              color: Colors.indigoAccent,
              title: 'Login', onClick: () => toLogin(context), icon: Icons.login),
            SizedBox(height: 10),
            AppButton(
              color: Theme.of(context).accentColor,
              title: 'New User', onClick: () => toRegister(context), icon: Icons.person)
          ],
        ),
      ),
    );
  }
}