import 'package:flutter/material.dart';
import 'package:messaging/helpers/helpers.dart';
import 'package:messaging/models/user_model.dart';
import 'package:messaging/services/auth_service.dart';
import 'package:messaging/services/response_handler.dart';
import 'package:messaging/theme.dart';
import 'package:messaging/views/home.dart';
import 'package:messaging/widgets/app_button.dart';
import 'package:provider/provider.dart';

class Login extends StatefulWidget {
  Login({Key? key}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {

  var formKey = GlobalKey<FormState>();

  String email = '';
  String password = '';

  void onSubmit() async {
    if(this.formKey.currentState!.validate()){
      this.formKey.currentState!.save();
      final ResponseData result = await AuthService.instance.login(email, password);
      if(result.success == true){
        context.read<UserModel>().setUser(result.data);
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => Home()));
      }else{
        showResultDialog(context, result.message);
      }
      
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Padding(
        padding: EdgeInsets.all(appPadding),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Login Now', style: Theme.of(context).textTheme.headline3!.copyWith(color: Colors.white)),
              SizedBox(height: 20),
              Form(
                key: formKey,
                child: Column(
                  children: [
                    TextFormField(
                      validator: (val){
                        if(val!.isEmpty){
                          return "Please Enter An Email Address";
                        }
                        if(!isValidEmail(val)){
                          return "Email format is not valid";
                        }
                      },
                      keyboardType: TextInputType.emailAddress,
                      onSaved: (val) => email = val.toString(),
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.white),
                        labelText: 'email'
                      ),
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      validator: (val){
                        if(val!.isEmpty){
                          return "Please enter your password";
                        }
                      },
                      obscureText: true,
                      obscuringCharacter: '-',
                      onSaved: (val) => password = val.toString(),
                      keyboardType: TextInputType.visiblePassword,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.white),
                        labelText: 'Password'
                      ),
                    ),
                    SizedBox(height: 20),
                    AppButton(color: Theme.of(context).accentColor, title: 'Continue', onClick: onSubmit, icon: Icons.login_rounded)
                  ]
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}