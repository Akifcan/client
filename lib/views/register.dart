import 'package:flutter/material.dart';
import 'package:messaging/helpers/helpers.dart';
import 'package:messaging/models/user_model.dart';
import 'package:messaging/services/auth_service.dart';
import 'package:messaging/services/response_handler.dart';
import 'package:messaging/theme.dart';
import 'package:messaging/views/home.dart';
import 'package:messaging/widgets/app_button.dart';
import 'package:provider/provider.dart';

class Register extends StatefulWidget {
  Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {

  var formKey = GlobalKey<FormState>();

  String username = '';
  String email = '';
  String password = '';

  void onSubmit() async {
    if(this.formKey.currentState!.validate()){
      this.formKey.currentState!.save();
      final ResponseData result = await AuthService.instance.register(username, password, email);
      if(result.success == true){
        context.read<UserModel>().setUser(result.data);
        Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => Home()));
      }else{
        showResultDialog(context, result.message);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: Padding(
        padding: EdgeInsets.all(appPadding),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text('Register Now', style: Theme.of(context).textTheme.headline3!.copyWith(color: Colors.white)),
              SizedBox(height: 20),
              Form(
                key: formKey,
                child: Column(
                  children: [
                    TextFormField(
                      validator: (val) => val!.isEmpty ? 'Please enter username ' : null,
                      onSaved: (val) => username = val.toString(),
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.white),
                        labelText: 'Username'
                      ),
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      validator: (val){
                        if(val!.isEmpty){
                          return "Please enter an email address";
                        }
                        if(!isValidEmail(val)){
                          return "Email format not valid";
                        }
                      },
                      onSaved: (val) => email = val.toString(),
                      keyboardType: TextInputType.emailAddress,
                      style: TextStyle(color: Colors.white),
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.white),
                        labelText: 'Email'
                      ),
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      validator: (val) => val!.isEmpty ? 'Please enter password ' : null,
                      onSaved: (val) => password = val.toString(),
                      obscureText: true,
                      style: TextStyle(color: Colors.white),
                      keyboardType: TextInputType.visiblePassword,
                      decoration: InputDecoration(
                        labelStyle: TextStyle(color: Colors.white),
                        labelText: 'Password'
                      ),
                    ),
                    SizedBox(height: 20),
                    AppButton(color: Theme.of(context).accentColor, title: 'Continue', onClick: onSubmit, icon: Icons.login_rounded),
                  ]
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}