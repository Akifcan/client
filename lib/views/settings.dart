import 'package:flutter/material.dart';
import 'package:messaging/services/auth_service.dart';
import 'package:messaging/views/change_avatar.dart';

class Settings extends StatelessWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
      body: Column(
        children: [
          DecoratedBox(
            decoration: BoxDecoration(
              color: Colors.red[900]
            ),
            child: ListTile(
              onTap: () => AuthService.instance.logout(context),
              title: Text('Logout', style: Theme.of(context).textTheme.headline6!.copyWith(color: Colors.white)),
              trailing: Icon(Icons.logout, color: Colors.white),
            ),
          ),
          ListTile(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => ChangeAvatar())),
              title: Text('Change Avatar', style: Theme.of(context).textTheme.headline6!.copyWith(color: Colors.black)),
              trailing: Icon(Icons.image, color: Colors.black),
            ),
        ],
      ),
    );
  }
}