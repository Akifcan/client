import 'package:flutter/material.dart';
import 'package:messaging/constants/constants.dart';
import 'package:messaging/models/user_model.dart';
import 'package:messaging/services/auth_service.dart';
import 'package:messaging/services/user_service.dart';
import 'package:provider/provider.dart';

class ConfirmAvatar extends StatelessWidget {
  final String imagePath;
  const ConfirmAvatar({Key? key, required this.imagePath}) : super(key: key);

  changeAvatar(BuildContext context)async{
    final result = await UserService.instance.changeAvatar(this.imagePath);
    context.read<UserModel>().changeAvatar(this.imagePath);
    if(result['data']['changeAvatar']['success']){
      AuthService.instance.updateToken(result['data']['changeAvatar']['token']);
      showDialog(context: context, builder: (_) => AlertDialog(
        title: Text('Success'),
        content: Text('Your avatar changed'),
        actions: [
          BackButton()
        ],
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Confirm avatar')),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(avatarPath('avatars/$imagePath.png'))
          )
        ),
      ),
      bottomNavigationBar: SizedBox(
              height: 50,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(0)
                  ),
                  primary: Theme.of(context).primaryColor
                ),
                child: Text('Set as a new avatar'),
                onPressed: () => changeAvatar(context),
              ),
            ),
    );
  }
}