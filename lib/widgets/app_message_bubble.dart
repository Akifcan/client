import 'package:flutter/material.dart';

class AppMessageBubble extends StatelessWidget {
  final String content;
  final AlignmentGeometry alignment;
  final bool isMe;
  const AppMessageBubble({Key? key, required this.alignment, required this.isMe, required this.content}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
              alignment: this.alignment,
              child: Container(
                margin: const EdgeInsets.symmetric(vertical: 5),
                padding: const EdgeInsets.all(20),
                width: MediaQuery.of(context).size.width / 1.5,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: isMe ? Theme.of(context).primaryColor : Theme.of(context).primaryColor.withOpacity(.8),
                ),
                child: Text(this.content, style: Theme.of(context).textTheme.headline6!.copyWith(color: Colors.white)),
              ),
            );
  }
}