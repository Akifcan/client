import 'package:flutter/material.dart';
import 'package:messaging/constants/constants.dart';
import 'package:messaging/models/messages_model.dart';
import 'package:messaging/views/messages.dart';


class AppLastMessages extends StatelessWidget {
  final MessagesModel message;
  const AppLastMessages({Key? key, required this.message}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  ListTile(
      onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (_) => Messages(message: this.message))),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(avatarPath(this.message.avatar)),
        radius: 30,
      ),
      title: Text(this.message.username, style: Theme.of(context).textTheme.headline5),
      subtitle: Text(this.message.content, style: Theme.of(context).textTheme.subtitle1!.copyWith(color: Colors.grey[600])),
      trailing: Icon(Icons.arrow_right, size: 40, color: Theme.of(context).accentColor),
    );

  }
}