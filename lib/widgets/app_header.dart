import 'package:flutter/material.dart';
import 'package:messaging/delegates/search_delegate.dart';
import 'package:messaging/theme.dart';

class AppHeader extends StatelessWidget {
  const AppHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
              flex: 1,
              child: Padding(
                padding: EdgeInsets.all(appPadding),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Chat', style: Theme.of(context).textTheme.headline3!.copyWith(color: Colors.white)),
                    Align(
                      alignment: Alignment.topLeft,
                      child: CircleAvatar(
                        backgroundColor: Theme.of(context).accentColor,
                        radius: 25,
                        child: IconButton(
                          tooltip: 'Search',
                          iconSize: 30,
                          onPressed: (){
                            showSearch(context: context, delegate: Search());
                          }, 
                          icon: Icon(Icons.search, color: Colors.white)
                        ),
                      ))
                  ],
                ),
              )
            );
  }
}