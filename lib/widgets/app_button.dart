import 'package:flutter/material.dart';
import 'package:messaging/theme.dart';

class AppButton extends StatelessWidget {
  final Color color;
  final String title;
  final VoidCallback onClick;
  final IconData icon;
  const AppButton({Key? key, required this.color, required this.title, required this.onClick, required this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
              padding: EdgeInsets.symmetric(horizontal: appPadding),
              width: double.infinity,
              child: ElevatedButton.icon(
                style: ElevatedButton.styleFrom(
                  primary: this.color,
                  padding: EdgeInsets.all(appPadding - 10)
                ),
                onPressed: this.onClick, 
                label: Text(this.title, style: Theme.of(context).textTheme.headline5!.copyWith(color: Colors.white)),
                icon: Icon(this.icon), 
              ));

  }
}