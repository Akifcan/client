import 'dart:convert';

import 'package:messaging/constants/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class UserService {
  UserService._privateConstructor();
  static final UserService _instance = UserService._privateConstructor();
  static UserService get instance => _instance;

  changeAvatar(String avatarPath) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    String? token = storage.getString(JWT_TOKEN_VARIABLE);
    var query = '''
      mutation CHANGE_AVATAR(\$avatar: String!){
        changeAvatar(avatar: \$avatar){
          success,
          token
        }
      } 
    ''';
    var response = await http.post(
        API_URL,
        headers: {
          "token": token.toString(),
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({"query": query, "variables": {"avatar": avatarPath}})
      );
      Map result = jsonDecode(response.body);
      return result;
  }
}