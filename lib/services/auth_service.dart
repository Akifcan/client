import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:messaging/constants/constants.dart';
import 'package:messaging/models/user_model.dart';
import 'package:messaging/services/response_handler.dart';
import 'package:messaging/views/home.dart';
import 'package:messaging/views/landing.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthService {
  AuthService._privateConstructor();
  static final AuthService _instance = AuthService._privateConstructor();
  static AuthService get instance => _instance;

  logout(BuildContext context) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    storage.remove(JWT_TOKEN_VARIABLE);
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => Landing()));
  }

  updateToken(String token) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    storage.setString(JWT_TOKEN_VARIABLE, token);
    print('new token is:'+storage.getString(JWT_TOKEN_VARIABLE).toString());
  }

  autoLogin(BuildContext context) async{
    SharedPreferences storage = await SharedPreferences.getInstance();
    String? token = storage.getString(JWT_TOKEN_VARIABLE);
    if(token != null){
      var query = '''
        query CURRENT_USER(\$token: String!){
            currentUser(token: \$token){
              email,
              username,
              avatar,
              _id
            }
          }
      ''';
      var response = await http.post(
        API_URL,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({"query": query, "variables": {"token": token}})
      );
      Map result = jsonDecode(response.body);
      context.read<UserModel>().setUser(result['data']['currentUser']);
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) => Home()));
    }
  }

  register(String username, String password, String email) async {
    var query = '''
      mutation REGISTER(\$email: String!, \$password: String!, \$username: String!) {
        register(register: { email: \$email, password: \$password, username: \$username}) {
          email,
          username,
          jwt,
          avatar,
          _id
        }
      }
    ''';
    var response = await http.post(
      API_URL,
      headers: {
        "Content-Type": "application/json",
        'Accept': 'application/json',
      },
      body: jsonEncode({"query": query, "variables": {"email": email, "password": password, "username": username}})
    );
    return responseHandler(response.body, 'register');
  }

  login(String email, String password) async {
    var query = '''
      mutation LOGIN(\$email : String!, \$password: String!) {
          login(login:{email:\$email, password:\$password}) {
            username,
            email,
            jwt,
            avatar,
            _id
          }
        }
    ''';
    var response = await http.post(
      API_URL,
      headers: {
        "Content-Type": "application/json",
        'Accept': 'application/json',
      },
      body: jsonEncode({"query": query, "variables": {"email": email, "password": password}})
    );
    return responseHandler(response.body, 'login');
  }
}