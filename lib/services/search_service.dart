import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:messaging/constants/constants.dart';

class SearchModel{
  final String email;
  final String avatar;
  final String username;
  SearchModel({required this.email, required this.avatar, required this.username});
  factory SearchModel.fromJson(Map json){
    return SearchModel(avatar: json['avatar'], email: json['email'], username: json['username']);
  }
}

class SearchService {
  SearchService._privateConstructor();
  static final SearchService _instance = SearchService._privateConstructor();
  static SearchService get instance => _instance;

  Future<List<SearchModel>> searchUser(String keyword) async {
    List<SearchModel> users = [];
    var query = '''
      query SEARCH_USER(\$keyword: String!) {
        searchUser(keyword: \$keyword){
          email,
          avatar,
          username
        }
      }
    ''';
    var response = await http.post(
        API_URL,
        headers: {
          "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({"query": query, "variables": {"keyword": keyword}})
      );
      Map result = jsonDecode(response.body);
      List userList = result['data']['searchUser'];
      users = userList.map((user) => SearchModel.fromJson(user)).toList();
      return users;
  }

}