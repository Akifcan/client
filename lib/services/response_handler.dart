import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:messaging/constants/constants.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ResponseData{
  String message;
  bool success;
  Map? data;
  ResponseData({required this.message, required this.success, this.data});
}

ResponseData responseHandler(var responseData, String objectName){
    Map json = jsonDecode(responseData);
    if(json['errors'] == null){
      print(json['data'][objectName]['jwt']);
      SharedPreferences.getInstance()
      .then((value){
        value.setString(JWT_TOKEN_VARIABLE, json['data'][objectName]['jwt']);
      });
      return ResponseData(message: 'Login Succees', success: true, data: json['data'][objectName]);
    }
    final String errorName = json['errors'][0]['extensions']['code'];
    print(errorName);
    if(errorName == 'FORBIDDEN'){
      return ResponseData(message: 'This user already exist', success: false);
    }
    if(errorName == 'UNAUTHENTICATED'){
      return ResponseData(message: 'Sorry but can\'t found user with this email', success: false);
    }
    return ResponseData(message: 'Unexcepted Error', success: false);
}

void showResultDialog(BuildContext context, String title){
  showDialog(context: context, builder: (_) => AlertDialog(
    title: Text(title),
    actions: [
      ElevatedButton.icon(onPressed: (){
        Navigator.of(context).pop();
      }, icon: Icon(Icons.arrow_left), label: Text('Back'))
    ],
  ));
}