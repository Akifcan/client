import 'dart:convert';

import 'package:messaging/constants/constants.dart';
import 'package:messaging/models/conversation_model.dart';
import 'package:messaging/models/messages_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;


class MessageService {
  MessageService._privateConstructor();
  static final MessageService _instance = MessageService._privateConstructor();
  static MessageService get instance => _instance;

  Future<List<MessagesModel>> listMessages({useDb = false}) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    String? token = storage.getString(JWT_TOKEN_VARIABLE);
    if(storage.getString(MESSAGES_CACHE) != null && !useDb){
      return _setMessages();
    }else{
      var query = '''
        query LIST_MESSAGES{
          listMessages {
            avatar,
            content,
            username,
            conversationId,
            _id
          }
        }
      ''';  
      var response = await http.post(API_URL, 
        headers: {
         "token": token.toString(),
         "Content-Type": "application/json",
          "Accept": "application/json"
        },
        body: jsonEncode({"query": query})
      );
      Map data = jsonDecode(response.body);
      print(data);
      storage.setString(MESSAGES_CACHE, jsonEncode(data['data']['listMessages']));
      return _setMessages();
    }
  }

  Future<List<ConversationModel>> listConversation(String conversationId, String userId, {useDb = false}) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    if(storage.getString(CONVERSATION_CACHE+conversationId) != null && !useDb){
      print('from cache!');
      List conversations = jsonDecode(storage.getString(CONVERSATION_CACHE+conversationId).toString());
      return conversations.map((e) {
        return ConversationModel(content: e['content'], isMe: e['sender'] == userId);
      }).toList();
    }else{
      print('from db');
      String token = storage.getString(JWT_TOKEN_VARIABLE).toString();
      var query = '''
        query LIST_CONVERSATIONS(\$conversationId: String!) {
            listConversations(conversationId: \$conversationId){
              content,
              users,
              sender,
              createdAt
            }
          }
      ''';
      var response = await http.post(
          API_URL,
          headers: {
            "token": token,
            "Content-Type": "application/json",
            "Accept": "application/json"
          },
          body: jsonEncode({"query": query, "variables": {"conversationId": conversationId}})
        );
      Map data = jsonDecode(response.body);
      List conversations = data['data']['listConversations'];
      storage.setString(CONVERSATION_CACHE+conversationId, jsonEncode(conversations));
      return conversations.map((e) {
        return ConversationModel(content: e['content'], isMe: e['sender'] == userId);
      }).toList();
    }
  }

  Future<List<MessagesModel>> _setMessages() async {
     SharedPreferences storage = await SharedPreferences.getInstance();
      List cache = jsonDecode(storage.getString(MESSAGES_CACHE).toString());
      List<MessagesModel> messages = cache.map((e) => MessagesModel.fromJson(e)).toList();
      return messages;
  }

  Future<bool> sendMessage(String receiver, String text) async {
    SharedPreferences storage = await SharedPreferences.getInstance();
    String token = storage.getString(JWT_TOKEN_VARIABLE).toString();

    var query = '''
      mutation SEND_MESSAGE(\$receiver: String!, \$content: String!) {
        sendMessage(message: {receiver: \$receiver, content: \$content}){
          content,
          conversationId
        }
      }
    ''';

   await http.post(
          API_URL,
          headers: {
            "token": token,
            "Content-Type": "application/json",
            "Accept": "application/json"
          },
          body: jsonEncode({"query": query, "variables": {"content": text, "receiver": receiver}})
   );
   return true;
  }

}