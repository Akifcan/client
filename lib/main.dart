import 'package:flutter/material.dart';
import 'package:messaging/globals.dart';
import 'package:messaging/models/messages_model.dart';
import 'package:messaging/models/user_model.dart';
import 'package:messaging/theme.dart';
import 'package:messaging/views/landing.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
         ChangeNotifierProvider(create: (_) => UserModel()),
         ChangeNotifierProvider(create: (_) => MessagesProvider())
      ],
      child: MyApp(),
    )
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      scaffoldMessengerKey: snackbarKey, // <= this
      theme: themeData,
      home: Landing(),
    );
  }
}