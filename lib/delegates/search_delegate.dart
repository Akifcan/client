import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:messaging/constants/constants.dart';
import 'package:messaging/services/search_service.dart';
import 'package:messaging/theme.dart';

class Search extends SearchDelegate{
  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(onPressed: (){
        query = '';
      }, icon: Icon(Icons.delete)),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return BackButton();
  }

  @override
  Widget buildResults(BuildContext context) {
    if(query.length > 0){
      SearchService.instance.searchUser(query);
    }
    return Padding(
      padding: EdgeInsets.all(appPadding),
      child: FutureBuilder<List<SearchModel>>(
        future: SearchService.instance.searchUser(query),
        builder: (_, snapshot) {
          if(!snapshot.hasData){
            return Center(child: CircularProgressIndicator());
          }
          if(snapshot.data!.length == 0){
            return Center(child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Text('Sorry', style: Theme.of(context).textTheme.headline3),
                Lottie.asset('assets/lottie/not-found.json', repeat: false),
                Text("We couldn't found this user", style: Theme.of(context).textTheme.headline5)
              ],
            ));
          }
          return ListView.builder(
            itemCount: snapshot.data!.length,
            itemBuilder: (_, index) => ListTile(
              leading: CircleAvatar(
                backgroundImage: NetworkImage(avatarPath(snapshot.data![index].avatar)),
              ),
              title: Text(snapshot.data![index].username),
              trailing: IconButton(
                tooltip: 'Send Message',
                onPressed: (){}, icon: Icon(Icons.send), color: Theme.of(context).primaryColor),
            ),
          );
        },

      )
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return SizedBox.shrink();
  }
}