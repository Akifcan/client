import 'package:flutter/material.dart';

ThemeData themeData = ThemeData(
  primaryColor: Colors.indigo,
  accentColor: Colors.orange,
  inputDecorationTheme: InputDecorationTheme(
    filled: true
  )
);

double appBorderRadius = 40;
double appPadding = 28.0;